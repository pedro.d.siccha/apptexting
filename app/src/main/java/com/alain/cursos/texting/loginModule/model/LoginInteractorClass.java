package com.alain.cursos.texting.loginModule.model;

import com.alain.cursos.texting.common.model.EventErrorTypeListener;
import com.alain.cursos.texting.common.pojo.User;
import com.alain.cursos.texting.loginModule.events.LoginEvent;
import com.alain.cursos.texting.loginModule.model.dataAccess.Authentication;
import com.alain.cursos.texting.loginModule.model.dataAccess.RealtimeDatabase;
import com.alain.cursos.texting.loginModule.model.dataAccess.StatusAuthCallback;
import com.google.firebase.auth.FirebaseUser;

import org.greenrobot.eventbus.EventBus;

/* *
 * Project: Texting from com.alain.cursos.texting.loginModule.model
 * Created by Alain Nicolás Tello on 25/10/2018 at 10:11 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public class LoginInteractorClass implements LoginInteractor {
    private Authentication mAuthentication;
    private RealtimeDatabase mDatabase;

    public LoginInteractorClass() {
        mAuthentication = new Authentication();
        mDatabase = new RealtimeDatabase();
    }

    @Override
    public void onResume() {
        mAuthentication.onResume();
    }

    @Override
    public void onPause() {
        mAuthentication.onPause();
    }

    @Override
    public void getStatusAuth() {
        mAuthentication.getStatusAuth(new StatusAuthCallback() {
            @Override
            public void onGetUser(FirebaseUser user) {
                post(LoginEvent.STATUS_AUTH_SUCCESS, user);

                mDatabase.checkUserExist(mAuthentication.getCurrentUser().getUid(), new EventErrorTypeListener() {
                    @Override
                    public void onError(int typeEvent, int resMsg) {
                        if (typeEvent == LoginEvent.USER_NOT_EXIST){
                            registerUser();
                        } else {
                            post(typeEvent);
                        }
                    }
                });
            }

            @Override
            public void onLauchUILogin() {
                post(LoginEvent.STATUS_AUTH_ERROR);
            }
        });
    }

    private void registerUser() {
        User currentUser = mAuthentication.getCurrentUser();
        mDatabase.registerUser(currentUser);
    }

    private void post(int typeEvent) {
        post(typeEvent, null);
    }

    private void post(int typeEvent, FirebaseUser user) {
        LoginEvent event = new LoginEvent();
        event.setTypeEvent(typeEvent);
        event.setUser(user);
        EventBus.getDefault().post(event);
    }
}
