package com.alain.cursos.texting.addModule.view;

/* *
 * Project: Texting from com.alain.cursos.texting.AddModule.view
 * Created by Alain Nicolás Tello on 14/11/2018 at 03:45 PM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface AddView {
    void enableUIElements();
    void disableUIElements();
    void showProgress();
    void hideProgress();

    void friendAdded();
    void friendNotAdded();
}
