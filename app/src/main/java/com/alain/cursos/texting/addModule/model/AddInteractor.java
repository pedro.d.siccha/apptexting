package com.alain.cursos.texting.addModule.model;

/* *
 * Project: Texting from com.alain.cursos.texting.AddModule.model
 * Created by Alain Nicolás Tello on 14/11/2018 at 03:51 PM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface AddInteractor {
    void addFriend(String email);
}
