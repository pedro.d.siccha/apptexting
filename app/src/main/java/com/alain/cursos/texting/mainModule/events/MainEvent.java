package com.alain.cursos.texting.mainModule.events;

import com.alain.cursos.texting.common.pojo.User;

/* *
 * Project: Texting from com.alain.cursos.texting.mainModule.events
 * Created by Alain Nicolás Tello on 02/11/2018 at 08:22 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public class MainEvent {
    public static final int USER_ADDED = 0;
    public static final int USER_UPDATED = 1;
    public static final int USER_REMOVED = 2;
    public static final int REQUEST_ADDED = 3;
    public static final int REQUEST_UPDATED = 4;
    public static final int REQUEST_REMOVED = 5;
    public static final int REQUEST_ACCEPTED = 6;
    public static final int REQUEST_DENIED = 7;
    public static final int ERROR_SERVER = 100;

    private User user;
    private int typeEvent;
    private int resMsg;

    public MainEvent() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(int typeEvent) {
        this.typeEvent = typeEvent;
    }

    public int getResMsg() {
        return resMsg;
    }

    public void setResMsg(int resMsg) {
        this.resMsg = resMsg;
    }
}
