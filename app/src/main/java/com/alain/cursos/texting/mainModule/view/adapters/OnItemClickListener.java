package com.alain.cursos.texting.mainModule.view.adapters;

import com.alain.cursos.texting.common.pojo.User;

/* *
 * Project: Texting from com.alain.cursos.texting.mainModule.view.adapters
 * Created by Alain Nicolás Tello on 09/11/2018 at 12:18 PM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface OnItemClickListener {
    void onItemClick(User user);
    void onItemLongClick(User user);

    void onAcceptRequest(User user);
    void onDenyRequest(User user);
}
