package com.alain.cursos.texting.common.model;

/* *
 * Project: Texting from com.alain.cursos.texting.common.model
 * Created by Alain Nicolás Tello on 02/11/2018 at 11:08 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface BasicEventsCallback {
    void onSuccess();
    void onError();
}
