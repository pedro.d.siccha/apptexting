package com.alain.cursos.texting.loginModule.events;

import com.google.firebase.auth.FirebaseUser;

/* *
 * Project: Texting from com.alain.cursos.texting.loginModule.events
 * Created by Alain Nicolás Tello on 25/10/2018 at 08:33 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public class LoginEvent {
    public static final int STATUS_AUTH_SUCCESS = 0;
    public static final int ERROR_SERVER = 100;
    public static final int STATUS_AUTH_ERROR = 101;
    public static final int USER_NOT_EXIST = 102;

    private FirebaseUser user;
    private int typeEvent;
    private int resMsg;

    public LoginEvent() {
    }

    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }

    public int getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(int typeEvent) {
        this.typeEvent = typeEvent;
    }

    public int getResMsg() {
        return resMsg;
    }

    public void setResMsg(int resMsg) {
        this.resMsg = resMsg;
    }
}
