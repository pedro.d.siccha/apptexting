package com.alain.cursos.texting.loginModule.model.dataAccess;

import android.support.annotation.NonNull;

import com.alain.cursos.texting.common.model.dataAccess.FirebaseAuthenticationAPI;
import com.alain.cursos.texting.common.pojo.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/* *
 * Project: Texting from com.alain.cursos.texting.loginModule.model.dataAccess
 * Created by Alain Nicolás Tello on 25/10/2018 at 08:47 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public class Authentication {
    private FirebaseAuthenticationAPI mAuthenticationAPI;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    public Authentication() {
        mAuthenticationAPI = FirebaseAuthenticationAPI.getInstance();
    }

    public void onResume(){
        mAuthenticationAPI.getmFirebaseAuth().addAuthStateListener(mAuthStateListener);
    }

    public void onPause(){
        if (mAuthStateListener != null){
            mAuthenticationAPI.getmFirebaseAuth().removeAuthStateListener(mAuthStateListener);
        }
    }

    public void getStatusAuth(final StatusAuthCallback callback){
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    callback.onGetUser(user);
                } else {
                    callback.onLauchUILogin();
                }
            }
        };
    }

    public User getCurrentUser(){
        return mAuthenticationAPI.getAuthUser();
    }
}
