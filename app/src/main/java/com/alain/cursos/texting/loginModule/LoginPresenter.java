package com.alain.cursos.texting.loginModule;

import android.content.Intent;

import com.alain.cursos.texting.loginModule.events.LoginEvent;

/* *
 * Project: Texting from com.alain.cursos.texting.loginModule
 * Created by Alain Nicolás Tello on 25/10/2018 at 08:30 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface LoginPresenter {
     void onCreate();
     void onResume();
     void onPause();
     void onDestroy();

     void result(int requestCode, int resultCode, Intent data);

     void getStatusAuth();

     void onEventListener(LoginEvent event);
}
