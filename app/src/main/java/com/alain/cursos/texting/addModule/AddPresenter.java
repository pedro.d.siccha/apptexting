package com.alain.cursos.texting.addModule;

import com.alain.cursos.texting.addModule.events.AddEvent;

/* *
 * Project: Texting from com.alain.cursos.texting.AddModule
 * Created by Alain Nicolás Tello on 14/11/2018 at 03:48 PM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface AddPresenter {
    void onShow();
    void onDestroy();

    void addFriend(String email);
    void onEventListener(AddEvent event);
}
