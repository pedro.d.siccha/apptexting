package com.alain.cursos.texting.mainModule.view;

import com.alain.cursos.texting.common.pojo.User;

/* *
 * Project: Texting from com.alain.cursos.texting.mainModule.view
 * Created by Alain Nicolás Tello on 02/11/2018 at 08:10 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface MainView {
    void friendAdded(User user);
    void friendUpdated(User user);
    void friendRemoved(User user);

    void requestAdded(User user);
    void requestUpdated(User user);
    void requestRemoved(User user);

    void showRequestAccepted(String username);
    void showRequestDenied();

    void showFriendRemoved();

    void showError(int resMsg);
}
