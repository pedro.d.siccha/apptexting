package com.alain.cursos.texting.common;

/* *
 * Project: Texting from com.alain.cursos.texting.common
 * Created by Alain Nicolás Tello on 02/11/2018 at 11:47 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public class Constants {
    public static final boolean ONLINE = true;
    public static final boolean OFFLINE = false;

    public static final long ONLINE_VALUE = -1;
}
