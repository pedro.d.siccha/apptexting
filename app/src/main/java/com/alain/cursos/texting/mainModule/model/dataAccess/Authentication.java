package com.alain.cursos.texting.mainModule.model.dataAccess;

import com.alain.cursos.texting.common.model.dataAccess.FirebaseAuthenticationAPI;

/* *
 * Project: Texting from com.alain.cursos.texting.mainModule.model.dataAccess
 * Created by Alain Nicolás Tello on 02/11/2018 at 09:12 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public class Authentication {
    private FirebaseAuthenticationAPI mAuthenticationAPI;

    public Authentication() {
        mAuthenticationAPI = FirebaseAuthenticationAPI.getInstance();
    }

    public FirebaseAuthenticationAPI getmAuthenticationAPI() {
        return mAuthenticationAPI;
    }

    public void signOff(){
        mAuthenticationAPI.getmFirebaseAuth().signOut();
    }
}
