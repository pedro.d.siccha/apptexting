package com.alain.cursos.texting.loginModule.view;

import android.content.Intent;

/* *
 * Project: Texting from com.alain.cursos.texting.loginModule.view
 * Created by Alain Nicolás Tello on 25/10/2018 at 08:25 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface LoginView {
     void showProgress();
     void hideProgress();

     void openMainActivity();
     void openUILogin();

     void showLoginSuccessfully(Intent data);
     void showMessageStarting();
     void showError(int resMsg);
}
