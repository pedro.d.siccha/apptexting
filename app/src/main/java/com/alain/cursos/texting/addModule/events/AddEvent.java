package com.alain.cursos.texting.addModule.events;

/* *
 * Project: Texting from com.alain.cursos.texting.AddModule.events
 * Created by Alain Nicolás Tello on 14/11/2018 at 03:50 PM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public class AddEvent {
    public static final int SEND_REQUEST_SUCCESS = 0;
    public static final int ERROR_SERVER = 100;

    private int typeEvent;

    public AddEvent() {
    }

    public int getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(int typeEvent) {
        this.typeEvent = typeEvent;
    }
}
