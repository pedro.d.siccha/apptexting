package com.alain.cursos.texting.mainModule;

import com.alain.cursos.texting.common.pojo.User;
import com.alain.cursos.texting.mainModule.events.MainEvent;

/* *
 * Project: Texting from com.alain.cursos.texting.mainModule
 * Created by Alain Nicolás Tello on 02/11/2018 at 08:14 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface MainPresenter {
    void onCreate();
    void onDestroy();
    void onPause();
    void onResume();

    void signOff();
    User getCurrentUser();
    void removeFriend(String friendUid);

    void acceptRequest(User user);
    void denyRequest(User user);

    void onEventListener(MainEvent event);
}
