package com.alain.cursos.texting.mainModule.model;

import com.alain.cursos.texting.common.pojo.User;

/* *
 * Project: Texting from com.alain.cursos.texting.mainModule.model
 * Created by Alain Nicolás Tello on 02/11/2018 at 08:23 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface MainInteractor {
    void subscribeToUserList();
    void unsubscribeToUserList();

    void signOff();

    User getCurrentUser();
    void removeFriend(String friendUid);

    void acceptRequest(User user);
    void denyRequest(User user);
}
