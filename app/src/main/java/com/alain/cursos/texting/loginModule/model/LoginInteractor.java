package com.alain.cursos.texting.loginModule.model;

/* *
 * Project: Texting from com.alain.cursos.texting.loginModule.model
 * Created by Alain Nicolás Tello on 25/10/2018 at 08:33 AM
 * All rights reserved 2018.
 * Course Specialize in Firebase for Android 2018 with MVP
 * More info: https://www.udemy.com/especialidad-en-firebase-para-android-con-mvp-profesional/
 */
public interface LoginInteractor {
     void onResume();
     void onPause();

     void getStatusAuth();
}
